package com.sigmotoa.resistorcalculator

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import kotlin.math.roundToLong

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val S1:Spinner = findViewById(R.id.S1)
        val S2:Spinner = findViewById(R.id.S2)
        val S3:Spinner = findViewById(R.id.S3)
        val S4:Spinner = findViewById(R.id.S4)

        val B1_2 = resources.getStringArray(R.array.B1_2)
        val B3 = resources.getStringArray(R.array.B3)
        val B4 = resources.getStringArray(R.array.B4)

        val A1_2 = ArrayAdapter(this, android.R.layout.simple_spinner_item, B1_2)
        val A3 =ArrayAdapter(this, android.R.layout.simple_spinner_item, B3)
        val A4 =ArrayAdapter(this, android.R.layout.simple_spinner_item, B4)

        S1.adapter = A1_2
        S2.adapter = A1_2
        S3.adapter = A3
        S4.adapter = A4

        var X1= "0"
        var X2 = "0"
        var X3 = 0.0
        var X4= 0.0

        S1.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                val band1 = findViewById<TextView>(R.id.band1)
                X1 = (p2).toString()
                when (p2) {
                    0 -> { band1.setBackgroundResource(R.color.black) }
                    1 -> { band1.setBackgroundResource(R.color.Brown) }
                    2 -> { band1.setBackgroundResource(R.color.Red) }
                    3 -> { band1.setBackgroundResource(R.color.Orange) }
                    4 -> { band1.setBackgroundResource(R.color.Yellow) }
                    5 -> { band1.setBackgroundResource(R.color.Green) }
                    6 -> { band1.setBackgroundResource(R.color.Blue) }
                    7 -> { band1.setBackgroundResource(R.color.purple_500) }
                    8 -> { band1.setBackgroundResource(R.color.Gray) }
                    9 -> { band1.setBackgroundResource(R.color.white) }
                    else -> { band1.setBackgroundResource(R.color.None) }
                }
                calcular(X1, X2, X3, X4)
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }

        S2.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                val band2 = findViewById<TextView>(R.id.band2)
                X2 = (p2).toString()
                when (p2) {
                    0 -> { band2.setBackgroundResource(R.color.black) }
                    1 -> { band2.setBackgroundResource(R.color.Brown) }
                    2 -> { band2.setBackgroundResource(R.color.Red) }
                    3 -> { band2.setBackgroundResource(R.color.Orange) }
                    4 -> { band2.setBackgroundResource(R.color.Yellow) }
                    5 -> { band2.setBackgroundResource(R.color.Green) }
                    6 -> { band2.setBackgroundResource(R.color.Blue) }
                    7 -> { band2.setBackgroundResource(R.color.purple_500) }
                    8 -> { band2.setBackgroundResource(R.color.Gray) }
                    9 -> { band2.setBackgroundResource(R.color.white) }
                    else -> { band2.setBackgroundResource(R.color.None) }
                }
                calcular(X1, X2, X3, X4)
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }

        S3.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                val band3 = findViewById<TextView>(R.id.band3)
                when (p2) {
                    0 -> { band3.setBackgroundResource(R.color.black)
                        X3 = 1.0 }
                    1 -> { band3.setBackgroundResource(R.color.Brown)
                        X3 = 10.0 }
                    2 -> { band3.setBackgroundResource(R.color.Red)
                        X3 = 100.0 }
                    3 -> { band3.setBackgroundResource(R.color.Orange)
                        X3 = 1000.0 }
                    4 -> { band3.setBackgroundResource(R.color.Yellow)
                        X3 = 10000.0 }
                    5 -> { band3.setBackgroundResource(R.color.Green)
                        X3 = 100000.0 }
                    6 -> { band3.setBackgroundResource(R.color.Blue)
                        X3 = 1000000.0 }
                    7 -> { band3.setBackgroundResource(R.color.purple_500)
                        X3 = 10000000.0 }
                    8 -> { band3.setBackgroundResource(R.color.Gray)
                        X3 = 100000000.0 }
                    9 -> { band3.setBackgroundResource(R.color.white)
                        X3 = 1000000000.0 }
                    10 -> { band3.setBackgroundResource(R.color.Gold)
                        X3 = 0.1 }
                    11 -> { band3.setBackgroundResource(R.color.Silver)
                        X3 = 0.01 }
                    else -> { band3.setBackgroundResource(R.color.None) }
                }
                calcular(X1, X2, X3, X4)
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }

        S4.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                val band4 = findViewById<TextView>(R.id.band4)
                when (p2) {
                    0 -> { band4.setBackgroundResource(R.color.Brown)
                        X4 = 0.01 }
                    1 -> { band4.setBackgroundResource(R.color.Red)
                        X4 = 0.02 }
                    2 -> { band4.setBackgroundResource(R.color.Gold)
                        X4 = 0.05 }
                    3 -> { band4.setBackgroundResource(R.color.Silver)
                        X4 = 0.1 }
                    else -> { band4.setBackgroundResource(R.color.None)
                        X4 = 0.2 }
                }
                calcular(X1, X2, X3, X4)
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }
    }

    @SuppressLint("SetTextI18n")
    fun calcular(X1:String, X2:String, X3:Double, X4:Double){
        val res2:TextView = findViewById(R.id.res2)
        val tol2:TextView = findViewById(R.id.tol2)
        if (X3<1000){
        val res: Long = ((X1 + X2).toDouble() * X3).roundToLong()
        res2.text = "$res Ω - Max:" + (res+(res*X4)).toString + "- Min:" + (res-(res*X4)).toString
        tol2.text = "mas o menos " + (X4*res).toString() + " Ω de tolerancia"
        }
        if (X3>=1000 && X3<1000000){
        val res: Long = ((X1 + X2).toDouble() * (X3/1000.0)).roundToLong()
        res2.text = "$res KΩ - Max:" + (res+(res*X4)).toString + " KΩ - Min:" + (res-(res*X4)).toString + " KΩ"
        tol2.text = "mas o menos " + (X4*res).toString() + " KΩ de tolerancia"
        }
        if (X3>=1000000){
        val res: Long = ((X1 + X2).toDouble() * (X3/1000000)).roundToLong()
        res2.text = "$res MΩ - Max:" + (res+(res*X4)).toString + " MΩ - Min:" + (res-(res*X4)).toString + " MΩ"
        tol2.text = "mas o menos " + (X4*res).toString() + " MΩ de tolerancia"
        }
    }
}
